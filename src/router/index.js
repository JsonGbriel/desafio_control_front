import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import Login from '@/views/Login'
import Service from '@/views/Service'
import ServiceOrder from '@/views/ServiceOrder'
import Report from '@/views/Report'
import checkLogged from '@/scripts/checkLogged'


Vue.use(VueRouter)

const routes = [
  {
    path: '/report',
    name: 'report',
    component: Report,
    beforeEnter: checkLogged,
  },
  {
    path: '/service',
    name: 'service',
    component: Service,
    beforeEnter: checkLogged,
  },
  {
    path: '/service-order',
    name: 'service-order',
    component: ServiceOrder,
    beforeEnter: checkLogged,
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    beforeEnter: checkLogged,
  },
  {
    path: '/',
    name: 'login',
    component: Login,
  },
]

const router = new VueRouter({
  routes
})

export default router
