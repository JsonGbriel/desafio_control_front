const checkLogged = (to, from, next) => {
    if (JSON.parse(localStorage.getItem('logged'))){
        next()
    }else{
        next('/')
    }
}

export default checkLogged