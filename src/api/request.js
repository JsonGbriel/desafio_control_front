import backend from '@/api/backend'
import axios from 'axios'


const default_config = {
    endpoint: '',
    method: '', 
    data: {},
}

function apiCall(config = default_config, callback_success = ()=>{}, callback_error = ()=>{}, auth = false, token = localStorage.getItem('user-token')){
    try {
        axios({
            'method': config.method,
            'url': backend.baseURL + config.endpoint,
            'data': config.data,
            'headers': auth ? { 'Content-Type':'application/json' } : { 'Content-Type':'application/json','Authorization': 'JWT ' + token }
        })
            .then((r)=>{
                callback_success(r)
            })
            .catch((r)=>{
                if (JSON.stringify(r).includes('401') || JSON.stringify(r).includes('403')){
                    localStorage.clear()
                    window.location.reload()
                }
                callback_error(r)
            });
    } catch (error){
        callback_error({status: error})
    }
}

export default apiCall
